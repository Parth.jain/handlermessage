package com.example.android.handlermessages;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {
    private final static int SET_PROGRESS_BAR_VISIBILITY = 0;
    private final static int PROGRESS_UPDATE = 1;
    private final static int SET_BITMAP = 2;
    private ImageView mImageView;
    private ProgressBar mProgressBar;
    private int mDelay = 500;


    static class UIHandler extends Handler {
        WeakReference<MainActivity> mParent;

        public UIHandler(WeakReference<MainActivity> parent) {
            mParent = parent;
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity parent = mParent.get();
            if (null != parent) {
                switch (msg.what) {
                    case SET_PROGRESS_BAR_VISIBILITY: {
                        parent.getProgressBar().setVisibility((Integer) msg.obj);
                        break;
                    }
                    case PROGRESS_UPDATE: {
                        parent.getProgressBar().setProgress((Integer) msg.obj);
                        break;
                    }
                    case SET_BITMAP: {
                        parent.getImageView().setImageBitmap((Bitmap) msg.obj);
                        break;
                    }
                }
            }
        }

    }

    Handler handler = new UIHandler(new WeakReference<MainActivity>(
            this));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = findViewById(R.id.imageView);
        mProgressBar = findViewById(R.id.progressBar);

        final Button loadBtn = findViewById(R.id.loadButton);
        loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new loadIconTask(R.drawable.painter, handler)).start();
            }
        });

        final Button otherBtn = findViewById(R.id.otherButton);
        otherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Other Button is Working", Toast.LENGTH_LONG).show();
            }
        });
    }

    private class loadIconTask implements Runnable {
        private final int resId;
        private Handler handler;

        public loadIconTask(int resId, Handler handler) {
            this.resId = resId;
            this.handler=handler;

        }

        @Override
        public void run() {
            Message msg = handler.obtainMessage(SET_PROGRESS_BAR_VISIBILITY, ProgressBar.VISIBLE);
            handler.sendMessage(msg);

            final Bitmap tmp = BitmapFactory.decodeResource(getResources(), resId);
            for (int i = 1; i < 11; i++) {
                sleep();
                msg = handler.obtainMessage(PROGRESS_UPDATE, i * 10);
                handler.sendMessage(msg);
            }

            msg = handler.obtainMessage(SET_BITMAP, tmp);
            handler.sendMessage(msg);

            msg = handler.obtainMessage(SET_PROGRESS_BAR_VISIBILITY,
                    ProgressBar.INVISIBLE);
            handler.sendMessage(msg);
        }

        private void sleep() {
            try {
                Thread.sleep(mDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    public ImageView getImageView() {
        return mImageView;
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

}


